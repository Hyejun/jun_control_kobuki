#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import serial
import time

if __name__ == '__main__':
    try:
        rospy.init_node('serial_read', anonymous=True)
        ser = serial.Serial('/dev/ttyACM0',9600)
	while not rospy.is_shutdown():
		var = ser.readline()
    	var = var.replace("\r\n","")
    	array_ultra = var.split('*')
    	array_ultra.pop(0)
    	print array_ultra
    	
    except rospy.ROSInterruptException:
        pass