#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"
#include "geometry_msgs/PoseStamped.h"
#include "cell.h"
ros::Publisher map_pub;

void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg){
  std_msgs::Header header = msg->header;
  nav_msgs::MapMetaData info = msg->info;
  ROS_INFO("Got map1 %d %d", info.width, info.height);
  Map map(info.width, info.height);
  for (unsigned int x = 0; x < info.width; x++)
    for (unsigned int y = 0; y < info.height; y++)
      map.Insert(Cell(x,y,msg->data[x+ info.width * y]));
  //map.GetCells();
  map.InsertStartNode(2,0);
  map.InsertEndNode(1,3);
  
  map.FindRoad();
  map.GetRoad(100,50);
//  nav_msgs::OccupancyGrid* newGrid = map.Grid();
// newGrid->header = header;
//  newGrid->info = info;
//  map_pub.publish(*newGrid);
  //map.GetCells();
//  map.InsertStartNode(0,0);
//  map.InsertEndNode(1,3);
//  while(!map.FindRoad());// 목표점에 도달할 때까지
//  map.FindRoad();
//  map.GetRoad(100,50);
}
void goalCB(const geometry_msgs::PoseStamped::ConstPtr& goal){
     
    ros::NodeHandle node;
    ROS_INFO("In ROS goal callback0 %f %f",goal->pose.position.x*100,goal->pose.position.y*100);
    double xx = (int)(goal->pose.position.x*100)/50;
    double yy = (int)(goal->pose.position.y*100)/50;
    ROS_INFO("In ROS goal callback1 %f %f",xx,yy);
    xx = 25+(xx*50);
    yy = 25+(yy*50);
    ROS_INFO("In ROS goal callback2 %f %f",xx,yy);
    node.setParam("goal_x", xx);
    node.setParam("goal_y", yy);

   }

int main(int argc, char **argv){
  ros::init(argc, argv, "grid");
  ros::NodeHandle n;
  ros::NodeHandle simple_nh("move_base_simple");
  //map_pub = n.advertise<nav_msgs::OccupancyGrid>("map_out",10);
  //ros::Subscriber map_sub = n.subscribe("map",10,mapCallback);
  ros::Subscriber goal_sub_ = simple_nh.subscribe("goal", 10,goalCB);

  ros::spin();
  return 0;
}
