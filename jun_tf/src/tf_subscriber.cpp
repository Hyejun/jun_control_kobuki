#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include "jun_tf/msg_Jun_tf.h"
#include <nav_msgs/Odometry.h>
#include <sstream>
#include <ecl/time.hpp>
#define M_PI       3.14159265
std::string turtle_name;
double origin_x =0;
double origin_y =0;
double origin_w =0;
double present_x =0;
double present_y =0;
double present_z =0;
double present_w =0;
double goal_x =0;
double goal_y =0;
double goal_w =0;
double temp_x =0;
double temp_y =0;
double temp_w =0;
double correction_x =0;
double correction_y =0;

ros::Publisher order_motor_pub;


void poseCallback(const nav_msgs::OdometryConstPtr& odom){
  ros::NodeHandle node;
  node.getParam("goal_x",goal_x);
  node.getParam("goal_y",goal_y);
  node.getParam("correction_x",correction_x);
  node.getParam("correction_y",correction_y);
  correction_x = correction_x*0.01;
  correction_y = correction_y*0.01;
  
 if( origin_x ==0 && origin_y ==0)
 {
   origin_x = odom->pose.pose.position.x;
   origin_y = odom->pose.pose.position.y;
 }
 else
 {
   present_x = odom->pose.pose.position.x;
   present_y = odom->pose.pose.position.y;
   present_z = odom->pose.pose.orientation.z;
   present_w = odom->pose.pose.orientation.w;
   //point_z = odom->pose.pose.position.z;
   
 }
   ROS_INFO("xxPresent %f cm ,%f cm %f degree\n",present_x*100,present_y*100,present_z*180);
   ROS_INFO("xxGOAL    %f cm ,%f cm\n",goal_x+origin_x*100,goal_y+origin_y*100);
   
/*
float64 goal_angle
float64 now_angle
float64 left_distance
*/
   temp_x = goal_x+origin_x*100 - (present_x+correction_x)*100;
   temp_y = goal_y+origin_y*100 - (present_y+correction_y)*100;
   
   jun_tf::msg_Jun_tf msg;      
   msg.goal_angle = atan2(temp_y,temp_x)*180/M_PI;
   if(msg.goal_angle<0)
   msg.goal_angle = 360+msg.goal_angle;
      
   msg.now_angle = 2*atan2(present_z,present_w)*180/M_PI;
  
   if(msg.now_angle<0)
   msg.now_angle = 360+msg.now_angle;
   //if(present_w<0)
   //msg.now_angle = 360-msg.now_angle;
   

   msg.left_distance = sqrt(temp_x*temp_x+temp_y*temp_y);  
   msg.present_xx = (correction_x+present_x)*100;
   msg.present_yy = (correction_y+present_y)*100;
   
   order_motor_pub.publish(msg);
    
}
int main(int argc, char** argv){
  ros::init(argc, argv, "tf_subscriber");
  ros::NodeHandle nh("~");
  tf::TransformBroadcaster broadcaster;

  
  if (argc != 2){
  turtle_name = "/turtle1";
  }
  ros::Rate loop_rate(10);//0.01s
  turtle_name = argv[1];
  ros::NodeHandle node;
  node.getParam("goal_x",goal_x);
  node.getParam("goal_y",goal_y);
  ROS_INFO("GOAL %f %f\n",goal_x,goal_y);
  ros::Subscriber sub = node.subscribe("odom", 1, &poseCallback);
  order_motor_pub = node.advertise<jun_tf::msg_Jun_tf>("order_motor", 1);
  while(1)
  {
	  ros::spinOnce();
	  loop_rate.sleep();
  }
  return 0;
};
