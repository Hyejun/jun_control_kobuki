/*****************************************************************************
 ** Ifdefs
 *****************************************************************************/

#ifndef CELL_H_
#define CELL_H_

#include "nav_msgs/OccupancyGrid.h"
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"
#include "ros/ros.h"
#include <vector>
#include <set>
#include <math.h>

  int start_x,start_y;
  int goal_x,goal_y;
  int present_x,present_y;

class Cell{
  
public:

  int x, y;
  int isempty; // 0 is empty
  double g ;
  double h ;
  Cell* parent_cell ; // parent
  

  Cell()
  {
	parent_cell = NULL;  	
  }

  Cell(int x, int y, int empty)
      : g(0),h(0), x(x), y(y), isempty(empty) 
  {
    setG();
	setH();
	parent_cell = NULL;
  }

  void calc()
  {
  	setG();
	setH();
  }
  void initCell(Cell *data)
  {
  	x = data->x;
  	y = data->y;
  	isempty = data->isempty;
  	setG();
	setH();
	parent_cell = NULL;
  }  
  void initCell(int lx,int ly, int empty)
  {
  	x = lx;
  	y = ly;
  	isempty = empty;
  	
  	setG();
	setH();
	parent_cell = NULL;
  }
  void setG(){
	g= abs(x-start_x)+abs(y-start_y);	
  }
  void setH(){
	h= sqrt((x-goal_x)*(x-goal_x)+(y-goal_y)*(y-goal_y));	
  }


};




class Map{

public:
  int width, height;
  int cell_count;
  std::vector<Cell> roadcells_vector; // final road_vector
  std::vector<Cell> roadcells_vector2;// final road_vector
  std::vector<Cell> open_vector;// open vector
  std::vector<Cell> close_vector;// close vector
  std::vector<Cell> allcells_vector;// all vector
  Map(int width, int height) 
      : width(width), height(height), cell_count(0){}
  ~Map(){
  }
  void Insert(Cell cell){
    cell_count++;
    allcells_vector.push_back(cell);
  }
  void GetCells()
  {
    for (int i = 0; i < width*height; i++)
    {
		ROS_INFO("Info Cell (%d,%d) %d",allcells_vector.at(i).x,allcells_vector.at(i).y,allcells_vector.at(i).isempty);
    }
  }
  Cell GetCell(int x,int y){
    ROS_INFO("XXInfo Cell (%d,%d) %d",allcells_vector.at(y + x * width).x,allcells_vector.at(y + x * width).y,allcells_vector.at(y + x * width).isempty);
    return allcells_vector.at(y + x * width);
  }
  void GetRoad(int gridSize, int cellSize)//100 ,50 insert data from vector to vector2
  {
  	int xxx = 0;
  	int yyy = 0;
    for (int i =0;i<roadcells_vector.size();i++)
    {
		ROS_INFO("ROAD Info Cell (%d,%d) %d",roadcells_vector.at(i).x,roadcells_vector.at(i).y,roadcells_vector.at(i).isempty);
		xxx = 25+roadcells_vector.at(i).x*50;
	   	yyy = 25+roadcells_vector.at(i).y*50;
		Cell l_cell(xxx,yyy,roadcells_vector.at(i).isempty);
		roadcells_vector2.push_back(l_cell);
    }
    for (int i =0;i<roadcells_vector2.size();i++)
    {
    	ROS_INFO("trans Info Cell (%d,%d)",roadcells_vector2.at(i).x,roadcells_vector2.at(i).y);
		
	}
  }

  
  int GetNeighbor(Cell pos, std::vector<Cell>& vecChilds)
  {
  	for (int i = 0; i < width*height; i++)
    {
		if( (abs(allcells_vector.at(i).x-pos.x)==1 && allcells_vector.at(i).y==pos.y )||(abs(allcells_vector.at(i).y-pos.y)==1 && allcells_vector.at(i).x==pos.x ))
		{
			allcells_vector.at(i).calc();
			vecChilds.push_back(allcells_vector.at(i));
			ROS_INFO("INSERT CHILD --Info Cell (%d,%d) %d %f %f",allcells_vector.at(i).x,allcells_vector.at(i).y,allcells_vector.at(i).isempty,allcells_vector.at(i).g,allcells_vector.at(i).h);
		}

    }
    return vecChilds.size();
  }


  void InsertStartNode(int x,int y)
  {
    start_x = x; 
    start_y = y;
  }

  void InsertEndNode(int x,int y)
  {
	goal_x = x; 
	goal_y = y;
  }
   //열린노드에 노드 삽입, 중복된 노드가 삽입되지 않도록 처리한다
  void InsertOpenNode(Cell pNode)
  {
   for (int i = 0; i<open_vector.size(); i++)
   {
    // if (open_vector.at(i).isSame(pNode.x,pNode.y))
    if (open_vector.at(i).x==pNode.x&&open_vector.at(i).y==pNode.y)    	
    {
	 ROS_INFO("NO INSERT (%d,%d) %d %f",pNode.x,pNode.y,pNode.isempty);
     return;
    }
   }
   ROS_INFO("InsertOpenNode Info Cell (%d,%d) %d %f",pNode.x,pNode.y,pNode.isempty);
   open_vector.push_back(pNode);
  }



 struct NodeCompare
{
  bool operator()(const Cell& user1, const Cell& user2)
  {
    return user1.g+user1.h < user2.g+user2.h;
  }
};

  void SortOpenNode()
  {
	
	ROS_INFO(" open_vector %d",open_vector.size());
	sort(open_vector.begin(),open_vector.end(),NodeCompare());

	for (int i = 0;i < open_vector.size(); i++)
    {
		//ROS_INFO("SortOpenNode1 Info Cell (%d,%d) %d %f %f",open_vector.at(i).x,open_vector.at(i).y,open_vector.at(i).isempty,open_vector.at(i).g+open_vector.at(i).h,open_vector.at(i).h);
    }
  }
  bool FindFromCloseNode(Cell pNode)
  {
	  for (int i = 0;i < close_vector.size(); i++)
	  {
	  	//ROS_INFO("FindFromCloseNode1 Info Cell (%d,%d) %d",close_vector.at(i).x,close_vector.at(i).y,close_vector.at(i).isempty);
	    //ROS_INFO("FindFromCloseNode2 Info Cell (%d,%d) %d",pNode.x,pNode.y,pNode.isempty);

	    // if (close_vector.at(i).isSame(pNode.x,pNode.y))
	    if(close_vector.at(i).x==pNode.x&&close_vector.at(i).y==pNode.y)
	    {
			ROS_INFO("FindFromCloseNode true");
			return true;    	
	    }
	  }
   return false;
  }


  bool FindRoad()
  {
  	Cell present_node = GetCell(start_x,start_y);
  	std::vector<Cell> vecChilds;
  	int iDepth = 0;
  	//ROS_INFO("__Info Cell (%d,%d) %d",start_node.x,start_node.y,start_node.isempty);
  	open_vector.push_back(present_node);
  	while(true)
  	{
	    if (open_vector.size() == 0)
	    {//만일 열린노드에 더이상 데이터가 없다면 길이 존재하지 않는것이다.
	      break;
	    }
	    present_node = open_vector.at(0);
  		open_vector.erase( open_vector.begin() );//erase first member 
  		ROS_INFO("Present Info Cell (%d,%d) %d",present_node.x,present_node.y,present_node.isempty);
	    
  		// if(present_node.isSame(goal_x,goal_y))
  		if(present_node.x==goal_x&&present_node.y==goal_y)
  		{
	     ROS_INFO("END");
	     while(allcells_vector.at(present_node.y + present_node.x * width).parent_cell!=NULL) //tracking it's parent node for it's parent is null
	     {
	      roadcells_vector.push_back(allcells_vector.at(present_node.y + present_node.x * width)); //add node to path list
	      present_node.initCell(allcells_vector.at(present_node.y + present_node.x * width).parent_cell); //get current node's parent
	     }
	     //for(int i=0;i<roadcells_vector.size();i++)
	     //	ROS_INFO("ROAD Info Cell (%d,%d) %d",roadcells_vector.at(i).x,roadcells_vector.at(i).y,roadcells_vector.at(i).isempty);
		return true;
  		}
  		//if not goal
  		close_vector.push_back(present_node);
	    ++iDepth;
  		vecChilds.clear();
  		GetNeighbor(present_node,vecChilds); // insert neighbor to vecChilds
  		for(int i=0;i<vecChilds.size();i++)
  		{
		    //ROS_INFO("_____ Info Cell (%d,%d) %d %f",close_vector.at(i).x,close_vector.at(i).y,close_vector.at(i).isempty);
		    if(vecChilds.at(i).isempty!=100)
		    {
		    	if(FindFromCloseNode(vecChilds.at(i)))
		    	{
		    		//ROS_INFO("continue Info Cell (%d,%d) %d %f",vecChilds.at(i).x,vecChilds.at(i).y,vecChilds.at(i).isempty);
		    		continue;
		    	}

		    	//ROS_INFO("XXXXXXXXXXXX Info Cell (%d,%d) %d %f",vecChilds.at(i).x,vecChilds.at(i).y,vecChilds.at(i).isempty);
		    	vecChilds.at(i).parent_cell = new Cell(present_node.x,present_node.y,present_node.isempty);
		    	allcells_vector.at(vecChilds.at(i).y + vecChilds.at(i).x * width).parent_cell = new Cell(present_node.x,present_node.y,present_node.isempty);
		    	InsertOpenNode(vecChilds.at(i));
		    	
		    }
		    
  		}
	  	



	  	SortOpenNode();
	  	  	//if(iDepth==8)
  			//break;
  	}
  	return false;

  }
};
#endif