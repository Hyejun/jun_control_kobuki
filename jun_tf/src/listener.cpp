#include "ros/ros.h"
#include "jun_tf/msg_Jun_tf.h"
#include <tf/transform_broadcaster.h>
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */

void chatterCallback(const jun_tf::msg_Jun_tf::ConstPtr& msg)
{
    ROS_INFO("recieve msg: %f ,%f, %f", msg->goal_angle,msg->now_angle,msg->left_distance);
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("order_motor", 10, chatterCallback);
  tf::TransformBroadcaster broadcaster;
  ros::Rate loop_rate(10);//0.01s
  double correction_x = 0;
  double correction_y = 0;
    
  while(1)
  {
    ros::spinOnce();
    loop_rate.sleep();

    n.getParam("correction_x",correction_x);
    n.getParam("correction_y",correction_y);
    correction_x = correction_x*0.01*(-1);
    correction_y = correction_y*0.01*(-1);
    broadcaster.sendTransform(
    tf::StampedTransform(
    tf::Transform(
    tf::Quaternion(0, 0, 0, 1),
    tf::Vector3(correction_x, correction_y, 0.0)),
    ros::Time::now(),
    "odom",
    "odom_xx"));

  }
  return 0;
}
