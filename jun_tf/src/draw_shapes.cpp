#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

int main( int argc, char** argv )
{
  ros::init(argc, argv, "draw_shapes");
  ros::NodeHandle n;
  ros::Rate r(1);
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 1);
 
  double goal_x =0;
  double goal_y =0;

  // Set our initial shape type to be a cube
  uint32_t shape = visualization_msgs::Marker::SPHERE;

  std::vector<double> roadcells_x;
  std::vector<double> roadcells_y;


  while (ros::ok())
  {
    n.getParam("goal_x",goal_x);
    n.getParam("goal_y",goal_y);
    n.getParam("road_x",roadcells_x);
    n.getParam("road_y",roadcells_y);
    visualization_msgs::Marker marker;
    visualization_msgs::Marker points;
    
    // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    points.header.frame_id = "/odom_xx";
    points.header.stamp = ros::Time::now();
    
    marker.header.frame_id = "/odom_xx";
    marker.header.stamp = ros::Time::now();

    // Set the namespace and id for this marker.  This serves to create a unique ID
    // Any marker sent with the same namespace and id will overwrite the old one
    points.ns = "draw_shapes";
    points.id = 0;

    marker.ns = "draw_shapes";
    marker.id = 1;



    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
    points.type = visualization_msgs::Marker::POINTS;
    marker.type = shape;

    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    points.action = visualization_msgs::Marker::ADD;
    marker.action = visualization_msgs::Marker::ADD;



    // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    marker.pose.position.x = goal_x*0.01;
    marker.pose.position.y = goal_y*0.01;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;


    // POINTS markers use x and y scale for width/height respectively
    points.scale.x = 0.1;
    points.scale.y = 0.1;
    points.scale.z = 0.1;
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 0.2;
    marker.scale.y = 0.2;
    marker.scale.z = 0.2;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = 1.0f;
    marker.color.g = 0.0f;
    marker.color.b = 0.0f;
    marker.color.a = 1.0;

    // Points are green
    points.color.g = 0.0f;
    points.color.b = 1.0f;
    points.color.a = 1.0f;

    // Input arrays
    ROS_INFO("xxxxxxxxxxxxxxxx");
    for (int i=0;i<roadcells_x.size();i++)
    {
      ROS_INFO("xxroad Cell (%f,%f)",roadcells_x.at(i),roadcells_y.at(i));
      geometry_msgs::Point p;
      p.x = roadcells_x.at(i)*0.01;
      p.y = roadcells_y.at(i)*0.01;
      p.z = 0;
      points.points.push_back(p);
    }
    marker.lifetime = ros::Duration();

    // Publish the marker
    while (marker_pub.getNumSubscribers() < 1)
    {
      if (!ros::ok())
      {
        return 0;
      }
      ROS_WARN_ONCE("Please create a subscriber to the marker");
      sleep(1);
    }
    marker_pub.publish(marker);
    marker_pub.publish(points);

    // Cycle between different shapes
    r.sleep();
  }
}
