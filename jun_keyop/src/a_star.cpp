/*****************************************************************************
 ** Includes
 *****************************************************************************/

#include "ros/ros.h"
#include "../include/keyop_core/a_star.hpp"


/*****************************************************************************
 ** Implementation
 *****************************************************************************/

/**
 * @brief Default constructor, needs initialisation.
 */
A_star::A_star() 
{
  ROS_INFO("A_STAR");
}

A_star::~A_star()
{

}

bool A_star::init()
{
  ros::NodeHandle nh("~");
  name = nh.getUnresolvedNamespace();
  return true;
}


/*****************************************************************************
 ** Implementation [Action]
 *****************************************************************************/
void A_star::readOccupancyGridService()
{

}
void A_star::readMap()
{

}


