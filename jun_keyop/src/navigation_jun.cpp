/*
 * Copyright (c) 2012, Yujin Robot.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Yujin Robot nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file /kobuki_keyop/src/keyop_core.cpp
 *
 * @brief Creates a node for remote controlling parts of robot_core.
 *
 **/

/*****************************************************************************
 ** Includes
 *****************************************************************************/

#include <ros/ros.h>
#include <ecl/time.hpp>
#include <ecl/exceptions.hpp>
#include <std_srvs/Empty.h>
#include <kobuki_msgs/MotorPower.h>
#include "../include/keyop_core/navigation_jun.hpp"


/*****************************************************************************
 ** Implementation
 *****************************************************************************/

/**
 * @brief Default constructor, needs initialisation.
 */
Navigation_jun::Navigation_jun() 
{
  ROS_INFO("THANK YOU");
  state = 0;
  node_count = 0;
  catchUltra = 0;
  isSearch = true;
}

Navigation_jun::~Navigation_jun()
{

}

bool Navigation_jun::init()
{
  ros::NodeHandle nh("~");

  name = nh.getUnresolvedNamespace();

  return true;
}

/*****************************************************************************
 ** Implementation [Action]
 *****************************************************************************/
int Navigation_jun::getAngularDirection()
{

  return angularDirection;
}

void Navigation_jun::setAngularDirection(double goal_angle,double now_angle,double left_distance)
{
    double interval = goal_angle - now_angle ;
    int thold = 0;  
    int return_value = 0;
    if( (interval<thold&&interval>-thold) || interval<(-360+thold) || interval> (360-thold) )
    {
	return_value= 0;
    }
    else if( (interval >0 &&interval<180) ||interval<-180)//각도조정 , 반시계
    {
	return_value= 1;
    }
    else if( (interval <0&&interval>-180) ||interval>180)//각도조정 , 시계
    {
	return_value= -1;
    }
  ROS_INFO("getAngular: %f ,%f, %f ,%f ,%d", goal_angle,now_angle,left_distance,interval,return_value);  //receive x,y
  angularDirection = return_value;
}


void Navigation_jun::setAngularDirection2(double xx,double yy,double now_angle)// for many nodes
{
//  double class_present_x;
//  double class_present_y;

    double temp_x = 0;
    double temp_y = 0;
    try {
       // ...
	temp_x = map_->roadcells_vector2.at(node_count).x - xx;
	temp_y = map_->roadcells_vector2.at(node_count).y - yy;
    } catch (...) {
       temp_x = 0;
       temp_y = 0;
       ROS_INFO("ERROR_setAngularDirection2");
    }


    double goal_angle = atan2(temp_y,temp_x)*180/M_PI;
    if(goal_angle<0)
    goal_angle = 360+goal_angle;

    double left_distance = sqrt(temp_x*temp_x+temp_y*temp_y);

    double interval = goal_angle - now_angle ;
    int thold = 0;  
    int return_value = 0;
    if( (interval<thold&&interval>-thold) || interval<(-360+thold) || interval> (360-thold) )
    {
	return_value= 0;
    }
    else if( (interval >0 &&interval<180) ||interval<-180)//각도조정 , 반시계
    {
	return_value= 1;
    }
    else if( (interval <0&&interval>-180) ||interval>180)//각도조정 , 시계
    {
	return_value= -1;
    }
  ROS_INFO("getAngular: %f ,%f, %f ,%f ,%d", goal_angle,now_angle,left_distance,interval,return_value);  //receive x,y
  angularDirection = return_value;
  
}

double* Navigation_jun::setPresent(double xx,double yy,double now_angle)
{
    p_present_x = xx;
    p_present_y = yy;
    p_present_angle = now_angle;
    ros::NodeHandle node;
    node.setParam("position_x",  p_present_x);
    node.setParam("position_y",  p_present_y);

    double temp_x = 0;
    double temp_y = 0;
    try {
       temp_x = map_->roadcells_vector2.at(node_count).x - xx;
       temp_y = map_->roadcells_vector2.at(node_count).y - yy;
       //temp_x = map_->roadcells_vector2.pop_back().x - xx;
       //temp_y = map_->roadcells_vector2.pop_back().y - yy;

    } catch (...) {
       temp_x = 0;
       temp_y = 0;
       ROS_INFO("ERROR_setPresent");
    }


    double goal_angle = atan2(temp_y,temp_x)*180/M_PI;
    if(goal_angle<0)
    goal_angle = 360+goal_angle;

    double left_distance = sqrt(temp_x*temp_x+temp_y*temp_y);
    
    double datas_present[2]= {goal_angle, left_distance};
    return datas_present;
}

bool Navigation_jun::nextNode()// 중간 목적지를 가지도록 한다.
{
      // ros::NodeHandle node;
      // node.setParam("position_x",  p_present_x);
      // node.setParam("position_y",  p_present_y);
      

   ROS_INFO("nodecount %d size %d",node_count,map_->roadcells_vector2.size());
  if(node_count+1< map_->roadcells_vector2.size())
  {	
	node_count++;
	return true;
  }
  else
  {
	ROS_INFO("END");
	return false;
  }
}

void Navigation_jun::setState(int lstate)
{
  state = lstate;
}
int Navigation_jun::getState()
{
  return state;
}

bool Navigation_jun::isChangedGoal()
{
  
  double local_goal_x = 0;
  double local_goal_y = 0;
  ros::NodeHandle node;
  node.getParam("goal_x",local_goal_x);
  node.getParam("goal_y",local_goal_y);
  //ROS_INFO("local_param %f %f",local_goal_x,local_goal_y);
  if(class_goal_x==local_goal_x&&class_goal_y==local_goal_y)// 둘다 같다
  {
	return false;
  }
  else// 다르다.
  {
	ROS_INFO("local_param %f %f",local_goal_x,local_goal_y);
	ROS_INFO("before class %f %f",class_goal_x,class_goal_y);

	class_goal_x = local_goal_x;
	class_goal_y = local_goal_y;
	ROS_INFO("goal %f %f",class_goal_x,class_goal_y);
  	return true;
  }
}
void Navigation_jun::loadMap(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  info = msg->info;
    
  for(int i=0;i< info.width*info.height;i++)
  {
	data[i] = msg->data[i];
	data_original[i] = msg->data[i];
	// 추후 맵설정에따라 바뀔수도있다..
  } 
}
void Navigation_jun::initMap()
{
  // 맵 초기화
  node_count = 0;
  roadcells_x.clear();
  roadcells_y.clear();

  // if(map_!=NULL)
  //   delete(map_);
ROS_INFO("INIT START");
  map_ = new Map(info.width, info.height);
  for (unsigned int x = 0; x < info.width; x++)
  {  
    for (unsigned int y = 0; y < info.height; y++)
    {  
	data[x+ info.width * y] = data_original[x+ info.width * y];
	map_->Insert(Cell(x,y,data[x+ info.width * y]));
    }
  }
ROS_INFO("INIT END");
// map_->GetCells();
}
void Navigation_jun::initMapForUltra(const std_msgs::Int16MultiArray::ConstPtr& array)
{
  // 맵 초기화
  node_count = 0;
  roadcells_x.clear();
  roadcells_y.clear();
  
  // if(map_!=NULL)
    // delete(map_);

  map_ = new Map(info.width, info.height);
  for (unsigned int x = 0; x < info.width; x++)
  {  
    for (unsigned int y = 0; y < info.height; y++)
    {  
  data[x+ info.width * y] = data_original[x+ info.width * y];
  map_->Insert(Cell(x,y,data[x+ info.width * y]));
    }
  }
  //ROS_INFO("theta %f x %f y %f",p_present_angle,p_present_x,p_present_y);
  ROS_INFO("catch START");
  catchObstacle(array);
  ROS_INFO("initMapForUltra END");

}
void Navigation_jun::drawRoad(double present_xx,double present_yy)
{
  ROS_INFO("drawRoad start %f %f %f %f",present_xx,present_yy,class_goal_x,class_goal_y);
  double px = (int)present_xx/50;
  double py = (int)present_yy/50;
  double gx = (int)class_goal_x/50;
  double gy = (int)class_goal_y/50;
  ROS_INFO("drawRoad start %f %f %f %f",px,py,gx,gy);
  map_->InsertStartNode(px,py);// start
  map_->InsertEndNode(gx,gy); // end
  
  if(map_->GetCell(px,py).isempty==100||map_->GetCell(gx,gy).isempty==100)
  {
    ROS_INFO("black param road_xy");
    ros::NodeHandle node;
    node.setParam("road_x", roadcells_x);
    node.setParam("road_y", roadcells_y);
    return;
  }
  map_->FindRoad();
  map_->GetRoad(100,50);
  //map_->removeViaNode();
	///////////////////////////////////////////

    for (std::vector<Cell>::iterator it = map_->roadcells_vector2.begin(); it != map_->roadcells_vector2.end(); it++)
    {
	//ROS_INFO("transform Info Cell (%d,%d) %d",it->x,it->y,it->value);
	roadcells_x.push_back(it->x);
	roadcells_y.push_back(it->y);
    }
    ROS_INFO("param road_xy");
    ros::NodeHandle node;
    node.setParam("road_x", roadcells_x);
    node.setParam("road_y", roadcells_y);
  
ROS_INFO("drawRoad end");
}
/*
 check width and height
 */
void Navigation_jun::loadUltra(const std_msgs::Int16MultiArray::ConstPtr& array)
{
      


      // if(array->data.at(2)<40||array->data.at(1)<20||array->data.at(3)<20)
      if(array->data.at(2)<50)
      {
          int xx = p_present_x/50;
          int yy = p_present_y/50;
          ROS_INFO("loadUltra");
   
          if(p_present_angle<15||p_present_angle>345)
          {
            if(  data[ xx+1+ info.width * yy]==0  )
            catchUltra++;
          }
          else if(p_present_angle>75&&p_present_angle<105)
          {
            if(data[xx+ info.width * (yy+1)]==0)
            catchUltra++;
          }
          else if(p_present_angle>165&&p_present_angle<195)
          {
            if(data[ xx-1+ info.width * yy]==0)
            catchUltra++;
          }
          else if (p_present_angle>255&&p_present_angle<285)
          {
            if(data[ xx+ info.width * (yy-1)]==0)
            catchUltra++;
          }
      }


}
void Navigation_jun::correctPosition(const std_msgs::Int16MultiArray::ConstPtr& array)
{
  int xx = p_present_x/50;
  int yy = p_present_y/50;
  ROS_INFO("correctPoistion -- position %d ,%d",xx,yy);
  ROS_INFO("vector %d, %d, %d, %d",array->data.at(0),array->data.at(2),array->data.at(4),array->data.at(6));
  
  if(xx ==5&& yy ==34)
  {
    ROS_INFO("___");
    double cx =0;
    ros::NodeHandle node;
    node.getParam("correction_x",cx);
    node.setParam("correction_x",  cx+ array->data.at(0) - 60);
  }


}


/*
 check width and height
 */
void Navigation_jun::catchObstacle(const std_msgs::Int16MultiArray::ConstPtr& array)
{
  int xx = p_present_x/50;
  int yy = p_present_y/50;
  try {
  if(p_present_angle<15||p_present_angle>345)
  {
    if(data[xx+1+ info.width * yy]==0)
    {
      ROS_INFO("add close_vector %d ,%d",xx+1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx+1)*info.height + yy) );
    }
    if(array->data.at(4)<40&&data[xx+ info.width * (yy+1)]==0)
    {
      ROS_INFO("add close_vector2 %d ,%d",xx,yy+1);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx)*info.height + yy+1) );
    }
    if(array->data.at(6)<40&&data[xx-1+ info.width * (yy)]==0)
    {
      ROS_INFO("add close_vector3 %d ,%d",xx-1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx-1)*info.height + yy) );
    }
    if(array->data.at(0)<40&&data[xx+ info.width * (yy-1)]==0)
    {
      ROS_INFO("add close_vector4 %d ,%d",xx,yy-1);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx)*info.height + yy-1) );
    }

  }
  else if(p_present_angle>75&&p_present_angle<105)
  {
    if(data[xx+ info.width * (yy+1)]==0)
    {
      ROS_INFO("add close_vector %d ,%d",xx,yy+1);      
      map_->close_vector.push_back( map_->allcells_vector.at(xx * info.height + (yy+1) ) );
    }
    if(array->data.at(4)<40&&data[xx-1+ info.width * (yy)]==0)
    {
      ROS_INFO("add close_vector2 %d ,%d",xx-1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx-1)*info.height + yy) );
    }
    if(array->data.at(6)<40&&data[xx+ info.width * (yy-1)]==0)
    {
      ROS_INFO("add close_vector3 %d ,%d",xx,yy-1);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx)*info.height + yy-1) );
    }
    if(array->data.at(0)<40&&data[xx+1+ info.width * (yy)]==0)
    {
      ROS_INFO("add close_vector4 %d ,%d",xx+1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx+1)*info.height + yy) );
    }


  }
  else if(p_present_angle>165&&p_present_angle<195)
  {
    if(data[xx-1+ info.width * yy]==0)
    {
      ROS_INFO("add close_vector %d ,%d",xx-1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at((xx -1)* info.height + yy ) );
    }
    if(array->data.at(4)<40&&data[xx+ info.width * (yy-1)]==0)
    {
      ROS_INFO("add close_vector2 %d ,%d",xx,yy-1);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx)*info.height + yy-1) );
    }
    if(array->data.at(6)<40&&data[xx+1+ info.width * (yy)]==0)
    {
      ROS_INFO("add close_vector3 %d ,%d",xx+1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx+1)*info.height + yy) );
    }
    if(array->data.at(0)<40&&data[xx+ info.width * (yy+1)]==0)
    {
      ROS_INFO("add close_vector4 %d ,%d",xx,yy+1);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx)*info.height + yy+1) );
    }
  }
  else if (p_present_angle>255&&p_present_angle<285)
  {
    if(data[xx+ info.width * (yy-1)]==0)
    {
      ROS_INFO("add close_vector %d ,%d",xx,yy-1);
      map_->close_vector.push_back( map_->allcells_vector.at(xx * info.height + (yy-1) ) );
    }
    if(array->data.at(4)<40&&data[xx+1+ info.width * (yy)]==0)
    {
      ROS_INFO("add close_vector2 %d ,%d",xx+1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx+1)*info.height + yy) );
    }
    if(array->data.at(6)<40&&data[xx+ info.width * (yy+1)]==0)
    {
      ROS_INFO("add close_vector3 %d ,%d",xx,yy+1);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx)*info.height + yy+1) );
    }
    if(array->data.at(0)<40&&data[xx-1+ info.width * (yy)]==0)
    {
      ROS_INFO("add close_vector4 %d ,%d",xx-1,yy);
      map_->close_vector.push_back( map_->allcells_vector.at(  (xx-1)*info.height + yy) );
    }
  } 
    
       // ...
    } catch (...) {
       ROS_INFO("ERROR_catchObstacle");
    }


 


}
int Navigation_jun::getcatchUltra()
{
 return catchUltra;
}


