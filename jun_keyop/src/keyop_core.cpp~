

/*****************************************************************************
 ** Includes
 *****************************************************************************/

#include <ros/ros.h>
#include <ecl/time.hpp>
#include <ecl/exceptions.hpp>
#include <std_srvs/Empty.h>
#include <kobuki_msgs/MotorPower.h>
#include "../include/keyop_core/keyop_core.hpp"

/*****************************************************************************
 ** Namespaces
 *****************************************************************************/

namespace keyop_core
{

/*****************************************************************************
 ** Implementation
 *****************************************************************************/

/**
 * @brief Default constructor, needs initialisation.
 */
KeyOpCore::KeyOpCore() : last_zero_vel_sent(true), // avoid zero-vel messages from the beginning
                         accept_incoming(true),
                         power_status(false),
                         wait_for_connection_(true),
                         cmd(new geometry_msgs::Twist()),
                         cmd_stamped(new geometry_msgs::TwistStamped()),
                         linear_vel_step(0.1),
                         linear_vel_max(3.4),
                         angular_vel_step(0.02),
                         angular_vel_max(1.2),
                         quit_requested(false),
                         key_file_descriptor(0)
{
  tcgetattr(key_file_descriptor, &original_terminal_state); // get terminal properties
  
}

KeyOpCore::~KeyOpCore()
{
  tcsetattr(key_file_descriptor, TCSANOW, &original_terminal_state);
}

/**
 * @brief Initialises the node.
 */
bool KeyOpCore::init()
{
  ros::NodeHandle nh("~");

  name = nh.getUnresolvedNamespace();

  /*********************
   ** Parameters
   **********************/
  
  nh.getParam("linear_vel_step", linear_vel_step);
  nh.getParam("linear_vel_max", linear_vel_max);
  nh.getParam("angular_vel_step", angular_vel_step);
  nh.getParam("angular_vel_max", angular_vel_max);
  nh.getParam("wait_for_connection", wait_for_connection_);

  ROS_INFO_STREAM("XXKeyOpCore : using linear  vel step [" << linear_vel_step << "].");
  ROS_INFO_STREAM("XXKeyOpCore : using linear  vel max  [" << linear_vel_max << "].");
  ROS_INFO_STREAM("XXKeyOpCore : using angular vel step [" << angular_vel_step << "].");
  ROS_INFO_STREAM("XXKeyOpCore : using angular vel max  [" << angular_vel_max << "].");

  navi_jun = new Navigation_jun();
  /*********************
   ** Subscribers
   **********************/
  keyinput_subscriber = nh.subscribe("teleop", 1, &KeyOpCore::remoteKeyInputReceived, this);
  jun_tf_subscriber_ = nh.subscribe("order_motor", 10, &KeyOpCore::orderMotorCallback,this);
  map_sub = nh.subscribe("map",10,&KeyOpCore::mapCallback,this);
  ultra_sub = nh.subscribe("ultra_msg",10,&KeyOpCore::arrayCallback,this);
  /*********************
   ** Publishers
   **********************/
  velocity_publisher_ = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  motor_power_publisher_ = nh.advertise<kobuki_msgs::MotorPower>("motor_power", 1);

  /*********************
   ** Velocities
   **********************/
  cmd->linear.x = 0.0;
  cmd->linear.y = 0.0;
  cmd->linear.z = 0.0;
  cmd->angular.x = 0.0;
  cmd->angular.y = 0.0;
  cmd->angular.z = 0.0;

   count_ordermotor = 0;
  /*********************
   ** Action
   **********************/
  isLoadMap = false; 
  auto_move_ = false;
  move_x = false; 
  move_z = false;
  pre_distance = 10000;
  blink_publisher_ = nh.advertise< kobuki_msgs::Led >("commands/led1", 10);
  bumper_event_subscriber_ = nh.subscribe("events/bumper", 10, &KeyOpCore::bumperEventCB, this);
  /*********************
   ** Wait for connection
   **********************/
  if (!wait_for_connection_)
  {
    return true;
  }
  ecl::MilliSleep millisleep;
  int count = 0;
  bool connected = false;
  while (!connected)
  {
    if (motor_power_publisher_.getNumSubscribers() > 0)
    {
      connected = true;
      break;
    }
    if (count == 6)
    {
      connected = false;
      break;
    }
    else
    {
      ROS_WARN_STREAM("KeyOp: could not connect, trying again after 500ms...");
      try
      {
        millisleep(500);
      }
      catch (ecl::StandardException e)
      {
        ROS_ERROR_STREAM("Waiting has been interrupted.");
        ROS_DEBUG_STREAM(e.what());
        return false;
      }
      ++count;
    }
  }
  if (!connected)
  {
    ROS_ERROR("KeyOp: could not connect.");
    ROS_ERROR("KeyOp: check remappings for enable/disable topics).");
  }
  else
  {
    kobuki_msgs::MotorPower power_cmd;
    power_cmd.state = kobuki_msgs::MotorPower::ON;
    motor_power_publisher_.publish(power_cmd);
    ROS_INFO("KeyOp: connected.");
    power_status = true;
  }

  // start keyboard input thread
  thread.start(&KeyOpCore::keyboardInputLoop, *this);
  return true;
}
/*****************************************************************************
 ** Implementation [Action]
 *****************************************************************************/
void KeyOpCore::ligthOn(int mode)
{
    kobuki_msgs::LedPtr led_msg_ptr;
    led_msg_ptr.reset(new kobuki_msgs::Led());     
    led_msg_ptr->value = mode;
    //led_msg_ptr->value = kobuki_msgs::Led::RED; 
    blink_publisher_.publish(led_msg_ptr);
}
void KeyOpCore::bumperEventCB(const kobuki_msgs::BumperEventConstPtr msg)
{
    // Preparing LED message
    kobuki_msgs::LedPtr led_msg_ptr;
    led_msg_ptr.reset(new kobuki_msgs::Led());

    if (msg->state == kobuki_msgs::BumperEvent::PRESSED)
    {
      ROS_INFO_STREAM("Bumper pressed. Turning LED on.");
      switch (msg->bumper)
     {
      case kobuki_msgs::BumperEvent::LEFT:
	//bumper_left_pressed_   = true;  
	led_msg_ptr->value = kobuki_msgs::Led::GREEN;
	ROS_INFO_STREAM("1");	
	break;
      case kobuki_msgs::BumperEvent::CENTER:  
	//bumper_center_pressed_ = true;  
	led_msg_ptr->value = kobuki_msgs::Led::RED;
	ROS_INFO_STREAM("2");
	break;
      case kobuki_msgs::BumperEvent::RIGHT:   
	//bumper_right_pressed_  = true;  
	led_msg_ptr->value = kobuki_msgs::Led::ORANGE;
	ROS_INFO_STREAM("3");
	break;
     }
	blink_publisher_.publish(led_msg_ptr);
    }
    else // kobuki_msgs::BumperEvent::RELEASED
    {
      ROS_INFO_STREAM("Bumper released. Turning LED off. ");
      led_msg_ptr->value = kobuki_msgs::Led::BLACK;
      blink_publisher_.publish(led_msg_ptr);
    }
}
/*

 */    
void KeyOpCore::orderMotorCallback(const jun_keyop::msg_Jun_tf::ConstPtr& msg)
{
    ros::NodeHandle node;
    node.getParam("ismove",ismove);
    node.getParam("angle",angle);
  
  if (power_status&&isLoadMap)
  {

  	if(navi_jun->isChangedGoal())// 목표값이 바뀌면 실행된다. 경로계획
  	{
      ROS_INFO("isChangedGoal");
  		navi_jun->initMap();
      navi_jun->drawRoad(msg->present_xx,msg->present_yy);
  		navi_jun->setState(0);
       cmd->linear.x = 0;
       cmd->angular.z = 0;
  		// draw nodes


  	}
	  if(ismove ==0)
	  {
	    cmd->angular.z = 0.0;
	    cmd->linear.x = 0.0;		
	    return;
	  }

    double goal_angle = navi_jun->setPresent(msg->present_xx,msg->present_yy,msg->now_angle)[0];
  	if(navi_jun->getState() == 5)
      goal_angle = angle;
    double interval = abs(goal_angle- msg->now_angle);
  	double left_distance = navi_jun->setPresent(msg->present_xx,msg->present_yy,msg->now_angle)[1];	
  	//ROS_INFO("info : %f ,%f, %f", goal_angle,msg->now_angle,left_distance);
  	switch(navi_jun->getState())
  	{
  		case 0://  no auto and only if goal changed
  		ROS_INFO("STATE 0**************************************************************");
  		navi_jun->setAngularDirection(goal_angle,msg->now_angle,left_distance);
  		navi_jun->setState(1);
  		break;

  		case 1:// only angle , interval이 감소하다 증가할 경우, state 0 으로 돌린다.
  		ROS_INFO("STATE 1 : %f ,%f, %f", goal_angle,msg->now_angle,left_distance);
  		cmd->linear.x = linear_vel_step*0;
      //virtual
      //cmd->angular.z = 2*navi_jun->getAngularDirection()*angular_vel_step;
  		//real
      cmd->angular.z = 3.0*navi_jun->getAngularDirection()*angular_vel_step;
      
      if(interval<20||interval>340)
      {
                  cmd->angular.z = 1.2*navi_jun->getAngularDirection()*angular_vel_step;
      }
      if(interval<10||interval>350)//속도를 줄인다.
      {
                  //virtual
                  //cmd->angular.z = 0.7*navi_jun->getAngularDirection()*angular_vel_step;
                  //real
                  cmd->angular.z = 0.8*navi_jun->getAngularDirection()*angular_vel_step;
      }
      //if(interval<0.5||interval>359.5)//각도 일치
  		if(interval<5||interval>355)//각도 일치
      {	
  			cmd->angular.z = 0;
  			navi_jun->setState(2);
  			ROS_INFO("ok_ready angle: %f ,%f, %f", goal_angle,msg->now_angle,left_distance);  //receive x,y
  		}	
  		break;
  		
      case 2:// move
  		navi_jun->isSearch = true;
      ROS_INFO("STATE 2 : %f ,%f, %f", goal_angle,msg->now_angle,left_distance);
  		cmd->linear.x = linear_vel_step*6;

  		if(left_distance <5)// 남은거리 3 이하면 종료
  		{
  			ROS_INFO("ok_END: %f ,%f, %f", goal_angle,msg->now_angle,left_distance);  //receive x,y
  			cmd->linear.x=0;
  			cmd->angular.z = 0;
  			navi_jun->setState(3);
  		}
      else if(left_distance<10)// velocity slow
      {
        cmd->linear.x = linear_vel_step*1.3;

      }
      else if(left_distance<15)// velocity slow
      {
        cmd->linear.x = linear_vel_step*3;
      }
      else if(left_distance<50)// velocity slow
      {
        cmd->linear.x = linear_vel_step*4;
      }
      if(interval>10&& interval<350)// too many angle
      {
        navi_jun->setState(0);
        cmd->linear.x=0;
        cmd->angular.z = 0;   
      }
  		break;
  		
      case 3: // 중간 node를 목적지로 해준다.
      /*
       * here is angle to 0,90,180,270 angle.
       * 
       */
      ROS_INFO("STATE 4: %f ,%f, %f", goal_angle,msg->now_angle,left_distance);  //receive x,y
      navi_jun->setState(4);

      break;

      case 4:
      // send robot position
      if(navi_jun->nextNode())
      {
        navi_jun->setState(0);
      }
      else
      {
        navi_jun->setState(5);
      }



      break;

      case 5: // rotation
      ROS_INFO("STATE 5 : %f ,%f, %f", goal_angle,msg->now_angle,left_distance);
      cmd->linear.x = linear_vel_step*0;
      //virtual
      //cmd->angular.z = 2*navi_jun->getAngularDirection()*angular_vel_step;
      //real
      cmd->angular.z = 3.0*navi_jun->getAngularDirection()*angular_vel_step;
      
      if(interval<20||interval>340)
      {
                  cmd->angular.z = 1.2*navi_jun->getAngularDirection()*angular_vel_step;
      }
      if(interval<10||interval>350)//속도를 줄인다.
      {
                  //virtual
                  //cmd->angular.z = 0.7*navi_jun->getAngularDirection()*angular_vel_step;
                  //real
                  cmd->angular.z = 0.8*navi_jun->getAngularDirection()*angular_vel_step;
      }
      //if(interval<0.5||interval>359.5)//각도 일치
      if(interval<5||interval>355)//각도 일치
      { 
        cmd->angular.z = 0;
        navi_jun->setState(7);
        ROS_INFO("ok_ready angle: %f ,%f, %f", goal_angle,msg->now_angle,left_distance);  //receive x,y
      } 
      

      break;


      case 6: // wait

      break;

      case 7: // correct



      break;

    }
  }
  else if(power_status)
  {
    //ROS_INFO("NO ROUTINE");  //receive x,y
       cmd->linear.x = 0;
       cmd->angular.z = 0;
  }

//resetVelocity()

}
// Map 받아오기
void KeyOpCore::mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg){
  nav_msgs::MapMetaData info = msg->info;
  ROS_INFO("Got map %d %d", info.width, info.height);
  navi_jun->loadMap(msg);
  isLoadMap = true;
  // navi_jun->initMap();
}
//ultra receive
void KeyOpCore::arrayCallback(const std_msgs::Int16MultiArray::ConstPtr& array){
    
    if(isLoadMap)
    navi_jun->loadUltra(array);


    if(navi_jun->getcatchUltra()>0&& navi_jun->getcatchUltra()<10&&navi_jun->isSearch)
    {
      navi_jun->setState(6);
      cmd->linear.x = 0;
      cmd->angular.z = 0;
      count_ordermotor ++;
      ROS_INFO("WAIT___ %d %d %d", navi_jun->isSearch, navi_jun->getcatchUltra(),count_ordermotor);
    }
    else if(navi_jun->getcatchUltra()>7&&navi_jun->isSearch)// wait 2sec and re mapping 
    {
      ROS_INFO("STATE 6!! %d %d %d", navi_jun->isSearch, navi_jun->getcatchUltra(),count_ordermotor);
      navi_jun->isSearch = false;
      navi_jun->catchUltra = 0;
      count_ordermotor = 0;
      navi_jun->initMapForUltra(array);
      navi_jun->drawRoad(navi_jun->p_present_x,navi_jun->p_present_y);
      navi_jun->setState(0);

    }
    if(count_ordermotor==10)
    {
      ROS_INFO("BACK TO STATE 0 %d %d %d", navi_jun->isSearch, navi_jun->getcatchUltra(),count_ordermotor);
      
      navi_jun->isSearch = false;
      navi_jun->catchUltra = 0;
      count_ordermotor = 0;
      navi_jun->setState(0);
    }

    if(navi_jun->getState()==7)
    {
      navi_jun->correctPosition(array);
    }

  
  
}

/*****************************************************************************
 ** Implementation [Spin]
 *****************************************************************************/

/**
 * @brief Worker thread loop; sends current velocity command at a fixed rate.
 *
 * It also process ros functions as well as aborting when requested.
 */
void KeyOpCore::spin()
{
  ros::Rate loop_rate(10);//0.01s

  while (!quit_requested && ros::ok())
  {
    // Avoid spamming robot with continuous zero-velocity messages
    if ((cmd->linear.x  != 0.0) || (cmd->linear.y  != 0.0) || (cmd->linear.z  != 0.0) ||
        (cmd->angular.x != 0.0) || (cmd->angular.y != 0.0) || (cmd->angular.z != 0.0))
    {
      velocity_publisher_.publish(cmd);
      last_zero_vel_sent = false;
      ROS_INFO_STREAM("xxxx" << cmd->linear.x << "|" << cmd->angular.z << "] "<<navi_jun->isSearch<<", "<<navi_jun->getcatchUltra());
    }
    else if (last_zero_vel_sent == false)
    {
      velocity_publisher_.publish(cmd);
      last_zero_vel_sent = true;
    }
    accept_incoming = true;
    ros::spinOnce();
    loop_rate.sleep();
  }
  if (quit_requested)
  { // ros node is still ok, send a disable command
    disable();
  }
  else
  {
    // just in case we got here not via a keyboard quit request
    quit_requested = true;
    thread.cancel();
  }
  thread.join();
}

/*****************************************************************************
 ** Implementation [Keyboard]
 *****************************************************************************/

/**
 * @brief The worker thread function that accepts input keyboard commands.
 *
 * This is ok here - but later it might be a good idea to make a node which
 * posts keyboard events to a topic. Recycle common code if used by many!
 */
void KeyOpCore::keyboardInputLoop()
{
  struct termios raw;
  memcpy(&raw, &original_terminal_state, sizeof(struct termios));

  raw.c_lflag &= ~(ICANON | ECHO);
  // Setting a new line, then end of file
  raw.c_cc[VEOL] = 1;
  raw.c_cc[VEOF] = 2;
  tcsetattr(key_file_descriptor, TCSANOW, &raw);

  puts("Reading from keyboard");
  puts("---------------------------");
  puts("Forward/back arrows : linear velocity incr/decr.");
  puts("Right/left arrows : angular velocity incr/decr.");
  puts("Spacebar : reset linear/angular velocities.");
  puts("d : disable motors.");
  puts("e : enable motors.");
  puts("r : auto move.");
  puts("q : quit.");
  char c;
  while (!quit_requested)
  {
    if (read(key_file_descriptor, &c, 1) < 0)
    {
      perror("read char failed():");
      exit(-1);
    }
    processKeyboardInput(c);
  }
}

/**
 * @brief Callback function for remote keyboard inputs subscriber.
 */
void KeyOpCore::remoteKeyInputReceived(const kobuki_msgs::KeyboardInput& key)
{
  processKeyboardInput(key.pressedKey);
}

/**
 * @brief Process individual keyboard inputs.
 *
 * @param c keyboard input.
 */
void KeyOpCore::processKeyboardInput(char c)
{
  /*
   * Arrow keys are a bit special, they are escape characters - meaning they
   * trigger a sequence of keycodes. In this case, 'esc-[-Keycode_xxx'. We
   * ignore the esc-[ and just parse the last one. So long as we avoid using
   * the last one for its actual purpose (e.g. left arrow corresponds to
   * esc-[-D) we can keep the parsing simple.
   */
  switch (c)
  {
    case kobuki_msgs::KeyboardInput::KeyCode_Left:
    {
      incrementAngularVelocity();
      break;
    }
    case kobuki_msgs::KeyboardInput::KeyCode_Right:
    {
      decrementAngularVelocity();
      break;
    }
    case kobuki_msgs::KeyboardInput::KeyCode_Up:
    {
      incrementLinearVelocity();
      break;
    }
    case kobuki_msgs::KeyboardInput::KeyCode_Down:
    {
      decrementLinearVelocity();
      break;
    }
    case kobuki_msgs::KeyboardInput::KeyCode_Space:
    {
      resetVelocity();
      navi_jun->setState(0);
      break;
    }
    case 'q':
    {
      quit_requested = true;
      ligthOn(kobuki_msgs::Led::BLACK);
      break;
    }
    case 'd':
    {
      disable();
      ligthOn(kobuki_msgs::Led::RED);
      break;
    }
    case 'e':
    {
      enable();
      ligthOn(kobuki_msgs::Led::GREEN);
      break;
    }
    case 'r': // get motor data..
    {
    	auto_move_ = !auto_move_;
    	if(auto_move_)
    	ROS_INFO("AUTO MOVE ON");
      
      if(ismove==0)
        ismove = 1;
      else
        ismove = 0;
      break;
    }
    default:
    {
      break;
    }
  }
}

/*****************************************************************************
 ** Implementation [Commands]
 *****************************************************************************/
/**
 * @brief Disables commands to the navigation system.
 *
 * This does the following things:
 *
 * - Disables power to the navigation motors (via device_manager).
 * @param msg
 */
void KeyOpCore::disable()
{
  cmd->linear.x = 0.0;
  cmd->angular.z = 0.0;
  velocity_publisher_.publish(cmd);
  accept_incoming = false;

  if (power_status)
  {
    ROS_INFO("KeyOp: die, die, die (disabling power to the device's motor system).");
    kobuki_msgs::MotorPower power_cmd;
    power_cmd.state = kobuki_msgs::MotorPower::OFF;
    motor_power_publisher_.publish(power_cmd);
    power_status = false;
  }
  else
  {
    ROS_WARN("KeyOp: Motor system has already been powered down.");
  }
}

/**
 * @brief Reset/re-enable the navigation system.
 *
 * - resets the command velocities.
 * - reenable power if not enabled.
 */
void KeyOpCore::enable()
{
  accept_incoming = false;

  cmd->linear.x = 0.0;
  cmd->angular.z = 0.0;
  velocity_publisher_.publish(cmd);

  if (!power_status)
  {
    ROS_INFO("KeyOp: Enabling power to the device subsystem.");
    kobuki_msgs::MotorPower power_cmd;
    power_cmd.state = kobuki_msgs::MotorPower::ON;
    motor_power_publisher_.publish(power_cmd);
    power_status = true;
  }
  else
  {
    ROS_WARN("KeyOp: Device has already been powered up.");
  }
}

/**
 * @brief If not already maxxed, increment the command velocities..
 */
void KeyOpCore::incrementLinearVelocity()
{
  if (power_status)
  {
    if (cmd->linear.x <= linear_vel_max)
    {
      cmd->linear.x += linear_vel_step;
    }
    ROS_INFO_STREAM("KeyOp: linear  velocity incremented [" << cmd->linear.x << "|" << cmd->angular.z << "]");
  }
  else
  {
    ROS_WARN_STREAM("KeyOp: motors are not yet powered up.");
  }
}

/**
 * @brief If not already minned, decrement the linear velocities..
 */
void KeyOpCore::decrementLinearVelocity()
{
  if (power_status)
  {
    if (cmd->linear.x >= -linear_vel_max)
    {
      cmd->linear.x -= linear_vel_step;
    }
    ROS_INFO_STREAM("KeyOp: linear  velocity decremented [" << cmd->linear.x << "|" << cmd->angular.z << "]");
  }
  else
  {
    ROS_WARN_STREAM("KeyOp: motors are not yet powered up.");
  }
}

/**
 * @brief If not already maxxed, increment the angular velocities..
 */
void KeyOpCore::incrementAngularVelocity()
{
  if (power_status)
  {
    if (cmd->angular.z <= angular_vel_max)
    {
      cmd->angular.z += angular_vel_step;
    }
    ROS_INFO_STREAM("KeyOp: angular velocity incremented [" << cmd->linear.x << "|" << cmd->angular.z << "]");
  }
  else
  {
    ROS_WARN_STREAM("KeyOp: motors are not yet powered up.");
  }
}

/**
 * @brief If not already mined, decrement the angular velocities..
 */
void KeyOpCore::decrementAngularVelocity()
{
  if (power_status)
  {
    if (cmd->angular.z >= -angular_vel_max)
    {
      cmd->angular.z -= angular_vel_step;
    }
    ROS_INFO_STREAM("KeyOp: angular velocity decremented [" << cmd->linear.x << "|" << cmd->angular.z << "]");
  }
  else
  {
    ROS_WARN_STREAM("KeyOp: motors are not yet powered up.");
  }
}

void KeyOpCore::resetVelocity()
{
  if (power_status)
  {
    cmd->angular.z = 0.0;
    cmd->linear.x = 0.0;
    ROS_INFO_STREAM("KeyOp: reset linear/angular velocities.");
  }
  else
  {
    ROS_WARN_STREAM("KeyOp: motors are not yet powered up.");
  }
}

} // namespace keyop_core
