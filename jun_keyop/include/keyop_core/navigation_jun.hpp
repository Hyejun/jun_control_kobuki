/*****************************************************************************
 ** Ifdefs
 *****************************************************************************/

#ifndef NAVIGATION_JUN_HPP_
#define NAVIGATION_JUN_HPP_

/*****************************************************************************
 ** Includes
 *****************************************************************************/

#include <ros/ros.h>
#include <ecl/threads.hpp>
#include "std_msgs/String.h"
#include "jun_keyop/msg_Jun_tf.h"
#include "nav_msgs/OccupancyGrid.h"
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"
#include "../include/keyop_core/cell.h"

#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Int16MultiArray.h"
/*****************************************************************************
 ** Namespaces
 *****************************************************************************/

/*****************************************************************************
 ** Interface
 *****************************************************************************/
/**
 * @brief x,y,theta 를 가지고 네비게이션을 위한 함수들의 모음이다.
 *
 */
class Navigation_jun
{
public:

  std::string name;
  const nav_msgs::OccupancyGrid::ConstPtr* msg_;
  nav_msgs::MapMetaData info;
  char data[4000];
  char data_original[4000];
  Map* map_;


  double p_present_x;
  double p_present_y;
  double p_present_angle;

  char ultra_data[8];
  int catchUltra;
  bool isSearch;

  /*********************
   ** C&D
   **********************/
  Navigation_jun();
  ~Navigation_jun();
  bool init();

  /*********************
   ** Runtime
   **********************/
  void setAngularDirection(double goal_angle,double now_angle,double left_distance);
  void setAngularDirection2(double xx,double yy,double now_angle);
  int getAngularDirection();
  void setState(int lstate);
  int getState();
  
  void setPresent(double xx,double yy);
  /*********************
   ** Map
   **********************/
  bool isChangedGoal();
  void loadMap(const nav_msgs::OccupancyGrid::ConstPtr& msg);
  void initMap();
  void initMapForUltra(const std_msgs::Int16MultiArray::ConstPtr& array);
  void drawRoad(double present_xx,double present_yy);
  double* setPresent(double present_xx,double present_yy,double now_angle);
  bool nextNode();
  

  
  /*********************
   ** Ultra
   **********************/
  void loadUltra(const std_msgs::Int16MultiArray::ConstPtr& array);
  void catchObstacle(const std_msgs::Int16MultiArray::ConstPtr& array);
  int getcatchUltra();
  void correctPosition(const std_msgs::Int16MultiArray::ConstPtr& array);


  

private:
/* 0 - 아무것도 안한다. 키보드로 조종
 * 1 - angular 만 조절한다.
 * 2 - angular and linear 를 조절한다.
 */
  int state;
  int angularDirection;
  double class_goal_x;
  double class_goal_y;
  double class_present_x;
  double class_present_y; 
  //double left_distance;
  int node_count;
  std::vector<double> roadcells_x;
  std::vector<double> roadcells_y;
  
  ecl::Thread thread;
  ecl::Thread order_thread;


  
};

#endif
