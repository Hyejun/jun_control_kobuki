/*
 * Copyright (c) 2012, Yujin Robot.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Yujin Robot nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file /include/teleop_core/keyop_core.hpp
 *
 * @brief The controlling node for remote operations on robot_core.
 *
 **/

/*****************************************************************************
 ** Ifdefs
 *****************************************************************************/

#ifndef KEYOP_CORE_NODE_HPP_
#define KEYOP_CORE_NODE_HPP_

/*****************************************************************************
 ** Includes
 *****************************************************************************/

#include <ros/ros.h>
#include <termios.h> // for keyboard input
#include <ecl/threads.hpp>
#include <geometry_msgs/Twist.h>  // for velocity commands
#include <geometry_msgs/TwistStamped.h>  // for velocity commands
#include <kobuki_msgs/KeyboardInput.h> // keycodes from remote teleops.
#include <kobuki_msgs/Led.h>
#include <kobuki_msgs/BumperEvent.h>
#include "std_msgs/String.h"
#include "jun_keyop/msg_Jun_tf.h"
#include "../include/keyop_core/navigation_jun.hpp"//for odometry read
////////////////////////////////////////////////////////////////////
// for map read
#include "nav_msgs/OccupancyGrid.h"
#include "std_msgs/Header.h"
#include "nav_msgs/MapMetaData.h"
#include "../include/keyop_core/cell.h"
//////////////////////////////////////////
//////////////////////////////////////////
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Int16MultiArray.h"

/*****************************************************************************
 ** Namespaces
 *****************************************************************************/

namespace keyop_core
{

/*****************************************************************************
 ** Interface
 *****************************************************************************/
/**
 * @brief Keyboard remote control for our robot core (mobile base).
 *
 */
class KeyOpCore
{
public:
  /*********************
   ** C&D
   **********************/
  KeyOpCore();
  ~KeyOpCore();
  bool init();

  /*********************
   ** Runtime
   **********************/
  void spin();
  
private:
  /////////////////////////////////////////////////////
  Navigation_jun *navi_jun;
  ros::Subscriber map_sub;
  ros::Subscriber ultra_sub;


  ////////////////////////////////////////////////////
  ros::Subscriber jun_tf_subscriber_;
  ros::Subscriber bumper_event_subscriber_;
  ros::Subscriber keyinput_subscriber;

  ////////////////////////////////////////////////////
  ros::Publisher velocity_publisher_;
  ros::Publisher motor_power_publisher_;
  ros::Publisher blink_publisher_;
  bool last_zero_vel_sent;
  bool accept_incoming;
  bool power_status;
  bool wait_for_connection_;
  
  geometry_msgs::TwistPtr cmd;
  geometry_msgs::TwistStampedPtr cmd_stamped;
  double linear_vel_step, linear_vel_max;
  double angular_vel_step, angular_vel_max;
  std::string name;

  int count_ordermotor;
  /*********************
   ** Commands
   **********************/
  void enable();
  void disable();
  void incrementLinearVelocity();
  void decrementLinearVelocity();
  void incrementAngularVelocity();
  void decrementAngularVelocity();
  void resetVelocity();

  /*********************
   ** Keylogging
   **********************/

  void keyboardInputLoop();
  void processKeyboardInput(char c);
  void remoteKeyInputReceived(const kobuki_msgs::KeyboardInput& key);
  void restoreTerminal();
  bool quit_requested;
  int key_file_descriptor;
  struct termios original_terminal_state;
  ecl::Thread thread;
  ecl::Thread order_thread;
  /*********************
   ** Action
   **********************/
  void ligthOn(int mode);
  void bumperEventCB(const kobuki_msgs::BumperEventConstPtr msg);
  void orderMotorCallback(const jun_keyop::msg_Jun_tf::ConstPtr& msg);
  bool auto_move_;
  bool move_x;
  bool move_z;
  double pre_distance;
  int ismove ;
  int angle ;
  /*********************
   ** Map
   **********************/
  void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
  Map *map_;
  bool isLoadMap;
  // ultra
  void arrayCallback(const std_msgs::Int16MultiArray::ConstPtr& array);
};

} // namespace keyop_core

#endif /* KEYOP_CORE_NODE_HPP_ */
